package wk2a;

public class LinkedListFastSize<E> extends LinkedList<E> {
    private int size = 0;

    @Override
    public int size() {
        System.out.println("Faster version");
        return size;
    }

    @Override
    public boolean add(E element) {
        ++size;
        return super.add(element);
    }
}
