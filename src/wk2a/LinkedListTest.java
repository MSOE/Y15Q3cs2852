package wk2a;

import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedListTest {

    @Test
    public void testAddOnEmpty() throws Exception {
        LinkedList<Integer> list = new LinkedList<>();
        list.add(0, 1);
        assertEquals((Integer)1, list.get(0));
    }

    @Test
    public void testAddAtBeginning() throws Exception {
        LinkedList<Integer> list = new LinkedList<>();
        list.add(0, 1);
        list.add(0, 2);
        assertEquals(2, (int)list.get(0));
    }
    @Test
    public void testAddAtEnd() throws Exception {

    }
    @Test
    public void testAddAtMiddle() throws Exception {

    }
}