package wk1a;

import java.util.Collection;
import java.util.List;
import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<>();
        words.add("First");
        words.add(null);
        words.add("Second");
        words.add("Third");
        words.remove(0);
        System.out.println(words.size());
        words.remove(null);
        System.out.println(words);
    }
}
