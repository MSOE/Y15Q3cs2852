package wk1a;

import java.util.*;

public class ArrayList<E> implements List<E> {
    private Object[] elements;

    public ArrayList() {
        elements = new Object[0];
    }

    @Override
    public int size() {
        return elements.length;
    }

    @Override
    public boolean isEmpty() {
        return size()==0;
    }

    @Override
    public boolean contains(Object target) {
        return indexOf(target)>=0;
    }

    @Override
    public Iterator<E> iterator() {
        return new ArrayListIterator();
    }

    private class ArrayListIterator implements Iterator<E> {
        private int index = -1;

        @Override
        public boolean hasNext() {
            return index<size()-1;
        }

        @Override
        public E next() {
            ++index;
            return (E)elements[index];
        }
    }

    private class ALListIterator implements ListIterator<E> {
        private double index = -0.5;
        private boolean okayToRemove = false;
        private boolean lastMoveForward = true;

        @Override
        public boolean hasNext() {
            return index<size()-1;
        }

        @Override
        public E next() {
            okayToRemove = true;
            lastMoveForward = true;
            ++index;
            return (E)elements[(int)index];
        }

        @Override
        public boolean hasPrevious() {
            return index>0;
        }

        @Override
        public E previous() {
            okayToRemove = true;
            lastMoveForward = false;
            --index;
            return (E)elements[(int)index];
        }

        @Override
        public int nextIndex() {
            return (int)(index+1);
        }

        @Override
        public int previousIndex() {
            return (int)index;
        }

        @Override
        public void remove() {
            if (okayToRemove == false) {
                throw new IllegalStateException("Can't remove with call to next() or previous()");
            }
            okayToRemove = false;
            if(lastMoveForward) {
                ArrayList.this.remove(previousIndex());
            }
        }

        @Override
        public void set(E e) {
            throw new UnsupportedOperationException("Didn't bother with this, so sorry.");
        }

        @Override
        public void add(E e) {
            throw new UnsupportedOperationException("Didn't bother with this, so sorry.");
        }
    }

    @Override
    public Object[] toArray() {
        return elements;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(E element) {
        Object[] newGuy = new Object[size()+1];
        for(int i=0; i<elements.length; ++i) {
            newGuy[i] = elements[i];
        }
        newGuy[size()] = element;
        elements = newGuy;
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        boolean changed = false;
        for(E element : c) {
            changed = add(element) || changed;
        }
        return changed;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        elements = new Object[0];
    }

    @Override
    public E get(int index) {
        return (E)elements[index];
    }

    @Override
    public E set(int index, E element) {
        E removed = (E)elements[index];
        elements[index] = element;
        return removed;
    }

    @Override
    public void add(int index, E element) {
        Object[] newElements = new Object[size()+1];
        for(int i=0; i<index; ++i) {
            newElements[i] = elements[i];
        }
        newElements[index] = element;
        for(int i=index; i<size(); ++i) {
            newElements[i+1] = elements[i];
        }
        elements = newElements;
    }

    @Override
    public boolean remove(Object target) {
        int objIndex=indexOf(target);
        boolean changed = false;
        if(objIndex>=0) {
            changed = true;
            remove(objIndex);
        }
        return changed;
    }

    @Override
    public E remove(int index) {
        Object[] newElements = new Object[size()-1];
        E removed = (E)elements[index];
        for(int j=0; j<index; j++){
            newElements[j] = elements[j];
        }
        for(int k=index+1; k<size(); k++){
            newElements[k-1] = elements[k];
        }
        elements = newElements;
        return removed;
    }

    @Override
    public String toString() {
        return "ArrayList{" +
                "elements=" + Arrays.toString(elements) +
                '}';
    }

    @Override
    public int indexOf(Object target) {
        int objIndex=-1;
        for(int i=0; objIndex!=-1 && i<size(); ++i) {
            if(elements[i]==target || (elements[i]!=null && elements[i].equals(target))){
                objIndex=i;
            }
        }
        return objIndex;
    }

    @Override
    public int lastIndexOf(Object target) {
        return 0;
    }

    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return null;
    }
}
