package wk3.s1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class LinkedList<E> implements List<E> {
    
    private Node head;
    private Node tail;

    public LinkedList() {
        head = null;
        tail = null;
    }

    @Override
    public int size() {
        int size = 0;
        Node walker = head;
        while(walker!=null) {
            ++size;
            walker = walker.next;
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head==null;
    }

    @Override
    public boolean contains(Object o) {
    	boolean contains = false;
        Node walker = head;
        while(walker != null) {
        	if (o.equals(walker.value)) {
        		contains = true;
        		break;
        	}
        	walker = walker.next;
        }
        return contains;
    }

    @Override
    public Iterator<E> iterator() {
        return new LinkedListIterator<E>();
    }

    @Override
    public Object[] toArray() {
    	Node walker = head;
    	List<Object> array = new ArrayList<Object>();
    	while (walker != null) {
    		array.add(walker.value); 
    		walker = walker.next;
    	}
        return array.toArray();
    }

    @SuppressWarnings("unchecked")
	@Override
    public <T> T[] toArray(T[] a) throws ArrayStoreException, NullPointerException {
    	Node walker = head;
    	try {
    		for (int i = 0; i < a.length && walker!= null; i++) {
    			a[i] = (T) walker.value;
    			walker = walker.next;
    		}
    	
    		if (walker != null) {
    			List<T> array = new ArrayList<T>();
    			for (T element : a) {
    				array.add(element);
    			}
    		
    			while (walker != null) {
    				array.add((T)walker.value); 
    				walker = walker.next;
    			}
    			
    			a = (T[]) array.toArray();
    		}
    	} catch (ClassCastException cce) {
			throw new ArrayStoreException("Type " + walker.value.getClass() +" cannot be cast to "+ a.getClass());
		}
        return a;
    }

    @Override
    public String toString() {
        Node walker = head;
        String string = "[";
        while(walker!=null) {
            string += walker.value + ", ";
            walker = walker.next;
        }
        return string.substring(0, string.length()-2) + "]";
    }

    @Override
    public boolean add(E element) {
        Node newGuy = new Node(element);
        if(isEmpty()) {
            head = newGuy;
            tail = newGuy;
        } else {
            tail.next = newGuy;
            tail = newGuy;
        }
        return true;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        head = tail = null;
    }

    private Node getNode(int index) {
        isInBounds(index);
        Node walker = head;
        for(int i=0; i<index; ++i) {
            walker = walker.next;
        }
        return walker;
    }

    @Override
    public E get(int index) {
        return getNode(index).value;
    }

    private void isInBounds(int index) {
        if(index<0 || index>=size()) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size());
        }
    }

    @Override
    public E set(int index, E element) {
        return null;
    }

    @Override
    public void add(int index, E element) {

    }

    @Override
    public E remove(int index) {
        isInBounds(index);
        E removed = get(index);
        if(index==0) {
            head = head.next;
        } else if(index==size()-1) {
            Node oneBefore = getNode(index-1);
            tail = oneBefore;
            oneBefore.next = null;
        } else {
            Node oneBefore = getNode(index-1);
            oneBefore.next = oneBefore.next.next;
        }
        return removed;
    }

    @Override
    public int indexOf(Object target) {
        int index = -1;
        for(int i=0; i<size() && index==-1; ++i) {
            E value = get(i);
            if(value==target || (value!=null && value.equals(target))) {
                index = i;
            }
        }
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return null;
    }

    // INNER CLASSES **************************************************************************************************
    private class Node {
        private E value;
        private Node next;

        private Node(E val) {
            this(val, null);
        }

        private Node(E val, Node nx) {
            value = val;
            next = nx;
        }
    }

    private class LinkedListIterator<T> implements Iterator<T> {

    	private Node currentNode = head;
    	
		@Override
		public boolean hasNext() throws NoSuchElementException {
			return (currentNode != null && currentNode.next != null);
		}

		@SuppressWarnings("unchecked")
		@Override
		public T next() {
			Node returnNode = null;
			if (currentNode != null) {
				returnNode = currentNode;
				currentNode = currentNode.next;
			} else {
				throw new NoSuchElementException("Iterator has reached its end, and cannot return another element.");
			}
			return (T) returnNode.value;
		}    	
    }
}