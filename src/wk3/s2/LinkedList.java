package wk3.s2;

import java.util.*;
import java.util.function.Consumer;

public class LinkedList<E> implements List<E> {

    private static class Node<E> {
        private E value;
        private Node<E> next;

        private Node(E val) {
            this(val, null);
        }

        private Node(E val, Node<E> nx) {
            value = val;
            next = nx;
        }
    }

    private Node<E> head;
    private Node<E> tail;

    public LinkedList() {
        head = null;
        tail = null;
    }

    @Override
    public int size() {
        int size = 0;
        Node<E> walker = head;
        while(walker!=null) {
            ++size;
            walker = walker.next;
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head==null;
    }

    @Override
    public boolean contains(Object target) {
        return indexOf(target)>=0;
    }

    private class LinkedListIterator implements Iterator<E>{
        private Node<E> currentNode;
        private Node<E> nextNode;

        public LinkedListIterator() throws NullPointerException{
            if (head!=null){
                nextNode = getNode(0);
            }else{
                throw new NullPointerException("The List is Empty");
            }
        }
        @Override
        public boolean hasNext(){
            return next()!=null;
        }

        @Override
        public E next() throws NoSuchElementException {
            if (nextNode.next!=null){
                currentNode = nextNode.next;
                nextNode = currentNode.next;
            }else {
                throw new NoSuchElementException("No more data :(");
            }
            return (E)currentNode.value;
        }

        @Override
        public void remove() throws UnsupportedOperationException{
            throw new UnsupportedOperationException("This Operation is not supported in this class");
        }

        @Override
        public void forEachRemaining(Consumer action) {
            while (hasNext()){
                action.accept(next());
            }
        }
    }
    @Override
    public Iterator<E> iterator() {
        LinkedListIterator iterator = new LinkedListIterator();
        return iterator;
    }

    @Override
    public Object[] toArray() {
        Object[] elements = new Object[size()];
        Node<E> walker = head;
        int i=0;
        while(walker!=null) {
            elements[i] = walker.value;
            ++i;
            walker = walker.next;
        }
        return elements;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public String toString() {
        Node<E> walker = head;
        String string = "[";
        while(walker!=null) {
            string += walker.value + ", ";
            walker = walker.next;
        }
        return string.substring(0, string.length()-2) + "]";
    }

    @Override
    public boolean add(E element) {
        Node<E> newGuy = new Node<E>(element);
        if(isEmpty()) {
            head = newGuy;
            tail = newGuy;
        } else {
            tail.next = newGuy;
            tail = newGuy;
        }
        return true;
    }

    @Override
    public boolean remove(Object target) {
        int index = indexOf(target);
        if(index>=0) {
            remove(index);
        }
        return index>=0;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        head = tail = null;
    }

    private Node<E> getNode(int index) {
        isInBounds(index);
        Node<E> walker = head;
        for(int i=0; i<index; ++i) {
            walker = walker.next;
        }
        return walker;
    }

    @Override
    public E get(int index) {
        return getNode(index).value;
    }

    private void isInBounds(int index) {
        if(index<0 || index>=size()) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size());
        }
    }

    @Override
    public E set(int index, E element) {
        Node<E> nodeToSet = getNode(index);
        E oldValue = nodeToSet.value;
        nodeToSet.value = element;
        return oldValue;
    }

    @Override
    public void add(int index, E element) {
        if(index!=size()) {
            isInBounds(index);
        }
        if(index==0) {
            head = new Node(element, head);
        } else {
            Node<E> oneBeforeInsert = getNode(index-1);
            oneBeforeInsert.next = new Node(element, oneBeforeInsert.next);
            if(index==size()-1) {
                tail = oneBeforeInsert.next;
            }
        }
    }

    @Override
    public E remove(int index) {
        isInBounds(index);
        E removed = get(index);
        if(index==0) {
            head = head.next;
        } else if(index==size()-1) {
            Node<E> oneBefore = getNode(index-1);
            tail = oneBefore;
            oneBefore.next = null;
        } else {
            Node<E> oneBefore = getNode(index-1);
            oneBefore.next = oneBefore.next.next;
        }
        return removed;
    }

    private boolean safeEquals(E value, Object target) {
        return value==target || (value!=null && value.equals(target));
    }

    @Override
    public int indexOf(Object target) {
        int index = -1;
        Node<E> walker = head;
        int count = 0;
        while(walker!=null && index==-1) {
            if(safeEquals(walker.value, target)) {
                index = count;
            }
            ++count;
            walker = walker.next;
        }
        return index;
    }

    @Override
    public int lastIndexOf(Object target) {
        int index = -1;
        Node<E> walker = head;
        int count = 0;
        while(walker!=null) {
            if(safeEquals(walker.value, target)) {
                index = count;
            }
            ++count;
            walker = walker.next;
        }
        return index;
    }



    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return null;
    }
}
