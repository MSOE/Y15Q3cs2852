package wk1b;

import java.util.*;

public class ArrayList<E> implements List<E> {
    private Object[] elements;

    public ArrayList() {
        elements = new Object[0];
    }

    @Override
    public int size() {
        return elements.length;
    }

    @Override
    public boolean isEmpty() {
        return size()==0;
    }

    @Override
    public boolean contains(Object target) {
        return indexOf(target)>=0;
    }

    @Override
    public Iterator<E> iterator() {
        return new ArrayListIterator();
    }

    private class ArrayListIterator implements Iterator<E> {

        private int index = -1;

        @Override
        public boolean hasNext() {
            return index<size()-1;
        }

        @Override
        public E next() {
            if(!hasNext()) {
                throw new NoSuchElementException("Sorry, no more data");
            }
            ++index;
            return (E)elements[index];
        }
    }

    @Override
    public Object[] toArray() {
        return elements;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(E element) {
        Object[] newElements = new Object[size()+1];
        for(int i=0; i<size(); ++i) {
            newElements[i] = elements[i];
        }
        newElements[size()] = element;
        elements = newElements;
        return true;
    }

    @Override
    public boolean remove(Object target) {
        int index = indexOf(target);
        boolean changed = false;
        if(index>=0) {
            remove(index);
            changed = true;
        }
        return changed;
    }

    @Override
    public String toString() {
        return "ArrayList{" +
                "elements=" + Arrays.toString(elements) +
                '}';
    }

    @Override
    public E remove(int index) {
        Object[] newArray = new Object[size()-1];
        E removed = (E)elements[index];
        for(int i = 0; i<size(); i++) {
            if (i < index) {
                newArray[i] = elements[i];
            } else if(i > index) {
                newArray[i-1] = elements[i];
            }
        }
        elements = newArray;
        return removed;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        elements = new Object[0];
    }

    @Override
    public E get(int index) {
        return (E)elements[index];
    }

    @Override
    public E set(int index, E element) {
        E removed = (E)elements[index];
        elements[index] = element;
        return removed;
    }

    @Override
    public void add(int index, E element) {
        Object[] newElements = new Object[size()+1];
        for(int i=0; i<index; ++i) {
            newElements[i] = elements[i];
        }
        newElements[index] = element;
        for(int i=index; i<size(); ++i) {
            newElements[i+1] = elements[i];
        }
        elements = newElements;
    }

    @Override
    public int indexOf(Object target) {
        int index = -1;
        for(int i = 0; index==-1 && i<size(); i++) {
            if (elements[i]==target || (elements[i]!=null && elements[i].equals(target))) {
                index = i;
            }
        }
        return index;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    private class ARListIterator implements ListIterator<E> {
        private int index = -1;
        private boolean canChange = false;
        private boolean lastMoveForward = false;

        @Override
        public boolean hasNext() {
            return index<size()-1;
        }

        @Override
        public E next() {
            if(!hasNext()) {
                throw new NoSuchElementException("Sorry, no more data");
            }
            canChange = true;
            lastMoveForward = true;
            ++index;
            return (E)elements[index];
        }

        @Override
        public boolean hasPrevious() {
            return index>=0;
        }

        @Override
        public E previous() {
            if(!hasPrevious()) {
                throw new NoSuchElementException("Sorry, no more data");
            }
            canChange = true;
            lastMoveForward = false;
            return (E)elements[index--];
        }

        @Override
        public int nextIndex() {
            return index+1;
        }

        @Override
        public int previousIndex() {
            return index;
        }

        @Override
        public void remove() {
            if(!canChange) {
                throw new IllegalStateException("Don't know what to remove");
            }
            canChange = false;
            if(lastMoveForward) {
                ArrayList.this.remove(index--);
            } else {
                ArrayList.this.remove(index+1);
            }

        }

        @Override
        public void set(E e) {
            throw new UnsupportedOperationException("Too lazy to implement, sorry");
        }

        @Override
        public void add(E e) {
            throw new UnsupportedOperationException("Too lazy to implement, sorry");
        }
    }

    @Override
    public ListIterator<E> listIterator() {
        return new ARListIterator();
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        ARListIterator li = new ARListIterator();
        li.index = index-1;
        return li;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return null;
    }
}
