package wk1b;


import java.util.Collection;

public class Driver {
    public static void main(String[] args) {
        Collection<String> words = new ArrayList<>();
        words.add("first");
        words.add("second");
        words.add(null);
        words.add("last");
        words.remove("last");
        System.out.println(words);
    }
}
