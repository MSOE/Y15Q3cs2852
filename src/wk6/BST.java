package wk6;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class BST<E extends Comparable<E>> implements Set<E> {
    private static class Node<E> {
        private E value;
        private Node<E> parent;
        private Node<E> lKid;
        private Node<E> rKid;

        public Node(E val, Node<E> parent, Node<E> lKid, Node<E> rKid) {
            value = val;
            this.parent = parent;
            this.lKid = lKid;
            this.rKid = rKid;
        }

        public Node(E val, Node<E> parent) {
            this(val, parent, null, null);
        }

        public Node(E value) {
            this(value, null, null, null);
        }
    }

    private Node<E> root;

    public BST() {
        root = null;
    }

    /*
            b      left rotate        a
         /    \        on a        /     \
       a       c        <---      w        b
      / \     / \                        /   \
     w   x   y   z                      x     c
                                             / \
                                            y   z
     */
    private Node<E> leftRotate(Node<E> a) {
        Node<E> b = a.rKid;
        Node<E> x = b.lKid;
        b.parent = a.parent;
        a.rKid = x;
        if(x!=null) {
            x.parent = a;
        }
        b.lKid = a;
        a.parent = b;
        if(b.parent!=null && b.parent.lKid == a) {
            b.parent.lKid = b;
        } else if(b.parent!=null) {
            b.parent.rKid = b;
        }
        return b;
    }

    @Override
    public int size() {
        return size(root);
    }

    private int size(Node<E> subroot) {
        return subroot==null ? 0 : 1 + size(subroot.lKid) + size(subroot.rKid);
    }

    @Override
    public boolean isEmpty() {
        return root==null;
    }

    @Override
    public boolean contains(Object target) {
        try {
            return contains((E) target, root);
        } catch (ClassCastException e) {
            // target is not comparable with elements in the tree,
            // so it clearly isn't contained in the tree
            return false;
        }
    }

    public int height() {
        return height(root);
    }

    private int height(Node<E> subroot) {
        return subroot==null ? 0 : 1 + Math.max(height(subroot.lKid), height(subroot.rKid));
    }

    private boolean contains(E target, Node<E> subroot) {
        boolean found = false;
        if(subroot!=null) {
            int comparison = subroot.value.compareTo(target);
            if (comparison == 0) {
                found = true;
            } else if (comparison > 0) { // check left
                found = contains(target, subroot.lKid);
            } else {
                found = contains(target, subroot.rKid);
            }
        }
        return found;
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(E element) {
        if(element==null) {
            throw new IllegalArgumentException("Do not support null elements");
        }
        boolean changed = false;
        if(root==null) {
            root = new Node(element);
            changed = true;
        } else {
            changed = add(element, root);
        }
        return changed;
    }

    private boolean add(E element, Node<E> subroot) {
        boolean changed = false;
        int comparison = subroot.value.compareTo(element);
        if (comparison > 0) { // check left
            if(subroot.lKid!=null) {
                changed = add(element, subroot.lKid);
            } else {
                subroot.lKid = new Node(element);
                changed = true;
            }
        } else if(comparison<0) {
            if(subroot.rKid!=null) {
                changed = add(element, subroot.rKid);
            } else {
                subroot.rKid = new Node(element);
                changed = true;
            }
        }
        return changed;
    }

    @Override
    public boolean remove(Object target) {
        if(target==null) {
            throw new IllegalArgumentException("Do not support null elements");
        }
        boolean changed = false;
        try {
            E element = (E)target;
            if(root!=null) {
                if(root.lKid==null && root.rKid==null) {
                    if(root.value.equals(element)) {
                        root = null;
                        changed = true;
                    }
                } else {
                    changed = remove(element, root);
                }
            }
        } catch(ClassCastException e) {
            changed = false;
        }
        return changed;
    }

    private boolean remove(E target, Node<E> subroot) {
        boolean changed = false;
        return changed;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        root = null;
    }

    @Override
    public String toString() {
        String result = toString(root);
        return result.length()>2 ? "[" + result.substring(0, result.length()-2) + "]" : "[]";
    }

    private String toString(Node<E> subroot) {
        String result = "";
        if(subroot!=null) {
            result += toString(subroot.lKid);
            result += subroot.value + ", ";
            result += toString(subroot.rKid);
        }
        return result;
    }

    // Pre-order: node, then left child, then right child
    // In-order: left child, then node, then right child
    // Post-order: left child, then right child, then node
    public static void main(String[] args) {
        BST<String> tree = new BST<>();
        tree.add("lunch");
        tree.add("cat");
        tree.add("dog");
        tree.add("sunshine");
        tree.add("rainbows");
        tree.remove("lunch");
        System.out.println(tree.size());
        System.out.println(tree);
    }
}















