package wk7;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class Trie implements Set<String> {
    public static void main(String[] args) {
        Trie tr = new Trie();
        tr.add("HAPPY");
        tr.add("HAP");
        tr.add("DAY");
        System.out.println(tr.contains("HAP"));
        System.out.println(tr.contains("CAP"));
        System.out.println(tr.contains("DAY"));
    }
    private static class Node {
        private boolean isWord;
        private Node[] branches = new Node[26];

        private Node(boolean isWord) {
            this.isWord = isWord;
        }

        private Node() {
            this(false);
        }

        private Node child(char letter) {
            return branches[childIndex(letter)];
        }

        private int childIndex(char letter) {
            return letter - 'A';
        }
    }

    private Node root;

    public Trie() {
        root = new Node();
    }

    @Override
    public int size() {
        return size(root);
    }

    private int size(Node subroot) {
        if(subroot==null) {
            return 0;
        }
        int size = 1;
        for(int i=0; i<subroot.branches.length; ++i) {
            size += size(subroot.branches[i]);
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size()==0;
    }

    @Override
    public boolean contains(Object target) {
        try {
            return contains((String)target, root);
        } catch (ClassCastException e) {
            return false;
        }
    }

    private boolean contains(String target, Node subroot) {
        if(subroot==null) {
            return false;
        }
        if(target.length()==0) {
            return subroot.isWord;
        }
        return contains(target.substring(1), subroot.child(target.charAt(0)));
    }

    @Override
    public Iterator<String> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(String word) {
        boolean changed = false;
        if(word!=null && word.length()>0) {
            changed = add(word, root);
        }
        return changed;
    }

    private boolean add(String word, Node subroot) {
        if(word.length()==0) {
            if(subroot.isWord) {
                return false;
            } else {
                subroot.isWord = true;
                return true;
            }
        }
        int index = subroot.childIndex(word.charAt(0));
        if(subroot.branches[index]==null) {
            subroot.branches[index] = new Node();
        }
        return add(word.substring(1), subroot.child(word.charAt(0)));
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends String> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }
}
