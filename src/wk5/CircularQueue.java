package wk5;

public class CircularQueue<E> implements PureQueue<E> {
    private Object[] data;
    private int front;
    private int back;
    private boolean isEmpty;
    private final static int DEFAULT_CAPACITY = 10;

    public CircularQueue() {
        this(DEFAULT_CAPACITY);
    }

    public CircularQueue(int capacity) {
        data = new Object[capacity];
        front = 0;
        back = 0;
        isEmpty = true;
    }

    @Override
    public boolean offer(E item) {
        if(front==back && !isEmpty) {
            return false;
        }
        data[back] = item;
        back = ++back%data.length;
        isEmpty = false;
        return true;
    }

    @Override
    public E peek() {
        return isEmpty ? null : (E)data[front];
    }

    @Override
    public E poll() {
        if(isEmpty) {
            return null;
        }
        int index = front;
        front = ++front%data.length;
        if(front==back) {
            isEmpty = true;
        }
        return (E)data[index];
    }
}
