package wk5;

public interface PureStack<E> {
    public E peek();
    public E pop();
    public void push(E element);
}
