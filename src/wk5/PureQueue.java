package wk5;

public interface PureQueue<E> {
    boolean offer(E item);
    E peek();
    E poll();
}
