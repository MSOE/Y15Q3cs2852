package wk5;

import wk2a.LinkedListFastSize;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.List;

public class Stack<E> implements PureStack<E> {
    private List<E> worker;

    public Stack() {
        worker = new LinkedList<>();
    }

    public Stack(int capacity) {
        worker = new ArrayList<>(capacity);
    }

    @Override
    public E peek() {
        return worker.get(worker.size() - 1);
    }

    @Override
    public E pop() {
        if (worker.isEmpty()) {
            throw new EmptyStackException();
        }
        return worker.remove(worker.size() - 1);
    }

    @Override
    public void push(E element) {
        worker.add(element);
    }

    public static void main(String[] args) {
        System.out.println(parenChecker("((a + b) + c)"));
        System.out.println(parenChecker("([a + b] + c)"));
        System.out.println(parenChecker("(a + {b + c})"));
        System.out.println(parenChecker("([a + b] + c"));
        System.out.println(parenChecker("([a + b] + c]"));
        System.out.println(parenChecker("(a + {b + c)"));
    }

    public static boolean parenChecker(String expr) {
        boolean legal = true;
        PureStack<Character> stack = new Stack<>();
        for(int i=0; legal && i<expr.length(); ++i) {
            char character = expr.charAt(i);
            try {
                switch (character) {
                    case '(':
                    case '[':
                    case '{':
                        stack.push(character);
                        break;
                    case ')':
                        if (stack.pop() != '(') {
                            legal = false;
                        }
                        break;
                    case ']':
                        if (stack.pop() != '[') {
                            legal = false;
                        }
                        break;
                    case '}':
                        if (stack.pop() != '{') {
                            legal = false;
                        }
                        break;
                }
            } catch(Exception e) {
                legal = false;
            }
        }
        try {
            stack.pop();
            legal = false;
        } catch(Exception e) {
            // ignore
        }
        return legal;
    }
}












