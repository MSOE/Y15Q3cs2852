package wk7b;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class Trie implements Set<String> {
    public static void main(String[] args) {
        Trie t = new Trie();
        t.add("stuff");
        t.add("some");
        t.add("1");
        System.out.println(t.contains("turkey"));
        System.out.println(t.contains("stuff"));
        System.out.println(t.contains("uff"));
        System.out.println(t.contains("som"));
    }

    private static class Node {
        private boolean isWord;
        private Node[] branches;
        private Node() {
            this(false);
        }
        private Node(boolean isWord) {
            this.isWord = isWord;
            branches = new Node[26];
        }
        private int indexOf(char letter) {
            letter = Character.toUpperCase(letter);
            if(letter<'A' || letter>'Z') {
                throw new IllegalArgumentException("Only accept characters in the alphabet.");
            }
            return letter - 'A';
        }
    }

    private Node root;

    public Trie() {
        root = new Node();
    }

    @Override
    public int size() {
        return size(root);
    }

    private int size(Node subroot) {
        int size = 0;
        if(subroot!=null) {
            size = subroot.isWord ? 1 : 0;
            for (Node node : subroot.branches) {
                size += size(node);
            }
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size()==0;
    }

    @Override
    public boolean contains(Object target) {
        boolean found = false;
        if(target!=null && target instanceof String) {
            found = contains((String)target, root);
        }
        return found;
    }

    private boolean contains(String target, Node subroot) {
        boolean found = false;
        if(subroot!=null) {
            if(target.length()==0) {
                found = subroot.isWord;
            } else {
                found = contains(target.substring(1), subroot.branches[subroot.indexOf(target.charAt(0))]);
            }
        }
        return found;
    }

    @Override
    public Iterator<String> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(String word) {
        boolean changed = false;
        if(word!=null && word.length()>0) {
            changed = add(word, root);
        }
        return changed;
    }

    private boolean add(String word, Node subroot) {
        boolean changed = false;
        if(word.length()==0) {
            if(!subroot.isWord) {
                subroot.isWord = true;
                changed = true;
            }
        } else {
            int index = subroot.indexOf(word.charAt(0));
            if(subroot.branches[index]==null) {
                subroot.branches[index] = new Node();
            }
            changed = add(word.substring(1), subroot.branches[index]);
        }
        return changed;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends String> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }
}
