package wk2b;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Driver {
    public static void main(String[] args) {
        List<String> words = new LinkedList<>();
        words.add("First");
        words.add("a");
        words.add("few");
        words.add("more");
        words.add("words");

        Iterator<String> itr = words.iterator();
        while(itr.hasNext()) {
            String word = itr.next();
               System.out.println(word);
        }
        itr.remove();
        for(String word : words) {
            System.out.println(word);
        }
    }
}
