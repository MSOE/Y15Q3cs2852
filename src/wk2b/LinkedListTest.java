package wk2b;

import org.junit.Test;
import wk2a.*;

import static org.junit.Assert.*;

public class LinkedListTest {

    @Test
    public void testAddOnEmpty() throws Exception {
        LinkedList<String> list = new LinkedList<>();
        list.add(0, "First");
        assertEquals("First", list.get(0));
    }

    @Test
    public void testAddToFront() throws Exception {
        LinkedList<String> list = new LinkedList<>();
        list.add(0, "First");
        list.add(0, "Firstest");
        assertEquals("Firstest", list.get(0));
    }
}