package wk8;

public class Stringy {
    private final String word;

    public Stringy(String word) {
        this.word = word;
    }

    @Override
    public int hashCode() {
        int hash = 1;
        for(int i=0; i<word.length(); ++i) {
            if(i%2==0) {
                hash *= word.charAt(i);
            } else {
                hash += word.charAt(i);
            }
        }
        return word.hashCode();
    }

    public static void main(String[] args) {
        Stringy w1 = new Stringy("The");
        Stringy w2 = new Stringy("rain");
        Stringy w3 = new Stringy("in");
        Stringy w4 = new Stringy("Spain");
        Stringy w5 = new Stringy("hsusiiaiskdkiaiskjjiaishearstays");
        Stringy w6 = new Stringy("jjaisiikasisiiskqpppqidhearstays");
        System.out.println(w1.hashCode());
        System.out.println(w2.hashCode());
        System.out.println(w3.hashCode());
        System.out.println(w4.hashCode());
        System.out.println(w5.hashCode());
        System.out.println(w6.hashCode());
    }
}








